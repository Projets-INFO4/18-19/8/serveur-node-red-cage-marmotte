# Marmot Cage back-end

This repository contains necessary files to build the **Docker** image for the Marmot Cage back-end. You'll find below the steps to follow in order to setup the *NodeRed* server.

## File hierarchy
```
├── README.md
├── flows.json
├── Dockerfile
└── config
    ├── set_password.sh
    └── settings.js
```

## <span id="step1">Step 1 : Building the docker image</span>

Change directory of your terminal to your cloned repository where the `Dockerfile` should be placed.
```bash
cd marmot_repo/
```
Ensure that your computer has a recent Docker version (around `v18.*.*`) and run the following commande :
```bash 
docker build -t marmot-image .
```
During this command, **Docker** will install every dependencies for the container. 
<br>
<span id="passwd"></span>
**N.B.** : If you took time to read the `Dockerfile`, you probably saw this line at the end of file :
```Dockerfile
# Setting admin and user password
RUN /data/set_password.sh MY_SUPER_ADMIN_SECRET MY_SUPER_USER_SECRET
```
>As you might think, this line allows you to customize admin and user passwords respectively named `MY_SUPER_ADMIN_SECRET` and `MY_SUPER_USER_SECRET` here. If you forget to change the default passwords, you need to re-build an image.

If this step failed because of a permission error on `set_password.sh`,  you need to modify rights for that file like this :
```bash
chmod 755 config/set_password.sh
```

## Step 2 : Run the container

Now that your **Docker** image is ready to run, let's start it with this command :
```bash
docker run -it -p 1880:1880 marmot-image
```
It should normally start the *NodeRed* server on port `1880`. To access the *NodeRed* dashboard, enter this address on your web browser : http://localhost:1880 or [http://\<remote-ip\>:1880]().
<br>
You'll then normally face a login page where you will need to use the credentials defined on the line we saw <a href="#passwd">before</a>.

## Step 3 : Setup MQTT broker

Before to use this flow, you need to configure MQTT credentials in the *node* **Application Rx**. For this, click on the button next to the server line and go in the security tab.
There you need to use these credentials :
  - **login** : `org-4`
  - **password** : `MZvLnTMWIIpSKLgZT4GAyXcE` 

> Note that if you are connected on the campus network like Eduroam or Wifi Campus, the server won't be able to connect to the broker.

## Step 4 : Setup Firebase

To be able to transmit data in the Firebase database, you need to provide to its node the following link :
```
https://cagesdatabase.firebaseio.com/
```
Modify also `Auth type` to `None`.

## Step 5 : Modify your flows

To modify your flows on this server, the way to do that is to export your flows in the file `flows.json` when you stop your container. To update these new flows, **you will need to re-build your image** (go back to <a href="#step1">Step 1</a>).