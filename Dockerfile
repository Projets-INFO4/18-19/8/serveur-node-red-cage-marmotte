# Using existing node-red Docker image
FROM nodered/node-red-docker

# Copying project config files in container data
COPY flows.json /data/
COPY config/settings.js /data/
COPY config/set_password.sh /data/

# Additional modules
RUN npm install node-red-admin
RUN npm install node-red-contrib-pubnub
RUN npm install node-red-contrib-ifttt
RUN npm install node-red-contrib-cayenne-mqtt-client
RUN npm install node-red-contrib-sms-free-mobile
RUN npm install node-red-contrib-firebase

# Setting admin and user password
RUN /data/set_password.sh MY_SUPER_ADMIN_SECRET MY_SUPER_USER_SECRET
