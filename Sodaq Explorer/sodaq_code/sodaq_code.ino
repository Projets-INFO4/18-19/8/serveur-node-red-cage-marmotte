#include <Sodaq_RN2483.h>

#define debugSerial SerialUSB
#define loraSerial Serial2

#define NIBBLE_TO_HEX_CHAR(i) ((i <= 9) ? ('0' + i) : ('A' - 10 + i))
#define HIGH_NIBBLE(i) ((i >> 4) & 0x0F)
#define LOW_NIBBLE(i) (i & 0x0F)

#define BUTTON_PIN 14 // the pin 14 links to our button

// Variables initialization
int buttonState = 0;
int count;

//Use OTAA, set to false to use ABP
bool OTAA = true;

// ABP
// USE YOUR OWN KEYS!
const uint8_t devAddr[4] =
{
    0x00, 0x00, 0x00, 0x00
};

// USE YOUR OWN KEYS!
const uint8_t appSKey[16] =
{
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
};

// USE YOUR OWN KEYS!
const uint8_t nwkSKey[16] =
{
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
};

// OTAA
// With using the GetHWEUI() function the HWEUI will be used
uint8_t DevEUI[8] =
{
  0x00, 0x04, 0xa3, 0x0b, 0x00, 0x23, 0xc8, 0xf0
};

const uint8_t AppEUI[8] =
{
  0x0c, 0xaf, 0xcb, 0x00, 0x00, 0xff, 0xff, 0xff
};

// 09 cf 4f 3c 04 00 12 34 28 ae d2 a6 ab f7 15 88, the key of our sodaq board
const uint8_t AppKey[16] =
{
0x09, 0xcf, 0x4f, 0x3c, 0x04, 0x00, 0x12, 0x34,
0x28, 0xae, 0xd2, 0xa6, 0xab, 0xf7, 0x15, 0x88
};

void setup()
{
  pinMode(BUTTON_PIN, INPUT); // Our button is an input
  delay(1000);
  randomSeed(analogRead(0));

  while ((!debugSerial) && (millis() < 10000)){
    // Wait 10 seconds for debugSerial to open
  }

  debugSerial.println("Start");

  // Start streams
  debugSerial.begin(57600);
  loraSerial.begin(LoRaBee.getDefaultBaudRate());

  LoRaBee.setDiag(debugSerial); // to use debug remove //DEBUG inside library
  LoRaBee.init(loraSerial, LORA_RESET);

  //Use the Hardware EUI
  getHWEUI();

  // Print the Hardware EUI
  debugSerial.print("LoRa HWEUI: ");
  for (uint8_t i = 0; i < sizeof(DevEUI); i++) {
    debugSerial.print((char)NIBBLE_TO_HEX_CHAR(HIGH_NIBBLE(DevEUI[i])));
    debugSerial.print((char)NIBBLE_TO_HEX_CHAR(LOW_NIBBLE(DevEUI[i])));
  }
  debugSerial.println();  

  setupLoRa();
}

void setupLoRa(){
  if(!OTAA){
    // ABP
    setupLoRaABP();
  } else {
    //OTAA
    setupLoRaOTAA();
  }
  // Uncomment this line to for the RN2903 with the Actility Network
  // For OTAA update the DEFAULT_FSB in the library
  // LoRaBee.setFsbChannels(1);

  LoRaBee.setSpreadingFactor(9);
}

void setupLoRaABP(){
  if (LoRaBee.initABP(loraSerial, devAddr, appSKey, nwkSKey, true))
  {
    debugSerial.println("Communication to LoRaBEE successful.");
  }
  else
  {
    debugSerial.println("Communication to LoRaBEE failed!");
  }
}

void setupLoRaOTAA(){

  if (LoRaBee.initOTA(loraSerial, DevEUI, AppEUI, AppKey, true))
  {
    debugSerial.println("Network connection successful.");
  }
  else
  {
    debugSerial.println("Network connection failed!");
  }
}

void loop()
{
  buttonState = digitalRead(BUTTON_PIN);  // We read the state of our button
  if(buttonState == HIGH) {               // If it is pressed
    if(count == 1) {       // used to avoid sending 300+ messages each time we press the button
        digitalWrite(LED_BUILTIN, LOW);   // We shut down the led while we are sending the message

        uint16_t nbcage = 12;             // CAGE NUMBER
        uint16_t tab[2];                  // data array for sending purpose

        tab[0] = nbcage;                  // cage number  
        tab[1] = 1;                       // cage state (1 means cage open, 0 closed)

          switch (LoRaBee.send(1, (uint8_t*)tab, sizeof(tab)))  //we send the data using LoRa
          {
          case NoError:
            debugSerial.println("Successful transmission.");
            break;
          case NoResponse:
            debugSerial.println("There was no response from the device.");
            break;
          case Timeout:
            debugSerial.println("Connection timed-out. Check your serial connection to the device! Sleeping for 20sec.");
            delay(20000);
            break;
          case PayloadSizeError:
            debugSerial.println("The size of the payload is greater than allowed. Transmission failed!");
            break;
          case InternalError:
            debugSerial.println("Oh No! This shouldn't happen. Something is really wrong! The program will reset the RN module.");
            setupLoRa();
            break;
          case Busy:
            debugSerial.println("The device is busy. Sleeping for 10 extra seconds.");
            delay(10000);
            break;
          case NetworkFatalError:
            debugSerial.println("There is a non-recoverable error with the network connection. The program will reset the RN module.");
            setupLoRa();
            break;
          case NotConnected:
            debugSerial.println("The device is not connected to the network. The program will reset the RN module.");
            setupLoRa();
            break;
          case NoAcknowledgment:
            debugSerial.println("There was no acknowledgment sent back!");
            break;
          default:
            break;
          }

          count = 0;
    }
    
  }else {
    digitalWrite(LED_BUILTIN, HIGH);
    count = 1;
    //sleep
  }
  
}



/**
* Gets and stores the LoRa module's HWEUI/
*/
static void getHWEUI()
{
  uint8_t len = LoRaBee.getHWEUI(DevEUI, sizeof(DevEUI));
}
